#emonlp.py
#Arnav Ghosh
#10th June 2016

import math
import numpy
from sklearn.cluster import KMeans
from gensim.models import Word2Vec
from PIL import Image

#Constants
APPROX_EMOJIS = 12 #integer > 0
IMAGE_WIDTH = 72
IMAGE_HEIGHT = 72
FORMAT = "png"
MAX_SIDE = 5

#Token (not needed for this cause model already created)
#punkt_token = nltk.data.load('tokenizers/punkt/english.pickle') will need it for breaking down sentences though
#(how does that tokenizer have to work: will it just break it down in sentences for now)

def loadModel(filename):
    """Returns a model previously trained using Word2Vec.
    Parameter: filename must be a string representing a
               valid model (trained using Word2Vec) present in the operting directory.
    """
    model = Word2Vec.load(filename)
    return model

def createCluster(model):
    """ Returns a dictionary in which emoji is mapped to
    the cluster number it is contained in.
    Parameter: model must be a model loaded and trained using
               methods available in Word2Vec.
    """
    word_vectors = model.syn0
    cluster_number = word_vectors.shape[0] / APPROX_EMOJIS
    kmeans = KMeans(n_clusters = cluster_number)
    idx = kmeans.fit_predict(word_vectors) #contains cluster assignment for emoji
    
    #contains each emoji mapped to it's cluster assignment
    word_map = dict(zip(model.index2word,idx))
    return word_map

def createBag(emoji_list, cluster):
    """Returns a numpy array representing a bag of centroids.
    
    An index i of the array represents centroid i. An integer at index i
    gives the number of words in word_list that belong to the cluster.
    
    Parameter: emoji_list is a list of emoji 
               cluster
    """
    num_possible = max(cluster.values()) + 1
    bag = numpy.zeros(num_possible, dtype = "float32")
     
    for emo in emoji_list:
        if emo in cluster:
            centroid = cluster[emo]
            bag[centroid] = bag[centroid] + 1
            
    print bag
    return bag

def bagsSent(sent_list, cluster):
    num_possible = max(cluster.values()) + 1
    bags_bags = numpy.zeros(((len(sent_list)), num_possible), dtype="float32")
    
    sent_num = 0
    for sent in sent_list:
        sent = list(sent)
        bags_bags[sent_num] = createBag(sent, cluster)
        sent_num = sent_num + 1
        
    return bags_bags

#mostly used for testing purposes
def printCluster(word_map):
    """ Given a kmeans clustering model, prints (as images) the emojis
    present in each cluster.  
    Precondition: word_map must be a dictionary in which each emoji is mapped to it's
                  cluster number.
    """
    emoji_list = word_map.keys()
    emoji_cluster_list = word_map.values()
    cluster_number = max(emoji_cluster_list)
    emoji_number = len(emoji_cluster_list)
    for cluster in xrange(0, cluster_number + 1):
        emojis = []
        clust_name = "Cluster %d" %cluster
        print "\n" + clust_name
        for index in xrange(0, emoji_number):
            if (emoji_cluster_list[index] == cluster):
                emojis.append(emoji_list[index])
        #if its the already created model, must first convert emoji names
        uni_list = convertUNI(emojis)  
        #save an image of them to see as well
        print emojis
        seeEmojis(uni_list,"Cluster_%d" +"s/" + clust_name) %APPROX_EMOJIS

#makes it easier to view evaluate clusters and the clustering methods       
def seeEmojis(emoji_list, name):
    """Returns a new image containing images of the emojis in a given list (repr. a sentence, cluster etc.)
    Each emoji image must be IMAGE_WIDTH x IMAGE_HEIGHT and of the format FORMAT. 
    Precondition: emoji_list must be a list in which each element is a string of the form:
                  u+xxxxx where xxxxx is code in Hex (eg:1f191).
                  NOTE: This implementation uses the codes of those emojis allowed on Twitter,
                        whose code consists of 4 or 5 alphanumeric characters. However, there are
                        certain emojis (such as flags) that have a very large code consisting of the two unicodes used to
                        represent them (eg: the chinese flag is given by 1f1e8-1f1f3 which has been shortened to
                        1f1e81f1f3 for these purposes)
                  name is a string representing the filename of the output image. DO NOT
                  include any file extension.
                  All corresponding image files must be named as code.FORMAT
    """
    #convert the list of unicode emojis into a string of filenames
    fname_list = []
    for emoji in emoji_list:
        code = emoji.lower()[2:]
        fname_list.append("72x72/" + code + "." + FORMAT)
    
    num_images = len(emoji_list)
    num_col = int(math.ceil(float(num_images)/MAX_SIDE))
    #create a new image that can hold MAX_SIDE images of IMAGE_WIDTH in a row.
    clust_image = Image.new('RGBA', (MAX_SIDE*IMAGE_WIDTH, num_col*IMAGE_HEIGHT))

    x = 0
    y = 0
    
    #rather than using two for loops, using one that is based on when images run out
    #making it easier to manage.
    for image in fname_list:
        im = Image.open(image)
        im.convert('RGBA')
        im = im.resize((IMAGE_WIDTH, IMAGE_HEIGHT))
        clust_image.paste(im, (x,y))
        x = x + IMAGE_WIDTH
        if x >= MAX_SIDE*IMAGE_WIDTH:
            x = 0
            y = y + IMAGE_HEIGHT
    
    clust_image.save("sent/" + name + "." + FORMAT)

#sepcifically designed for conversions necessary with a downloaded eoji model
def convertUNI(emoji_list):
    """Returns a list of unicodes based on a list in which each element is of the form
    eojixxxx/xxxxx.
    Precondition: emoji_list must be a list in which each element is a string of the
                  form eojixxxx/xxxxx.
    """
    uni_list = []
    for potential in emoji_list:
        uni_list.append("u+" + potential[4:])
    return uni_list

def convertEoji(emoji_list):
    """Returns a list of eojixxxx/xxxxx codes based on a list in which each element is of the form
    u+xxxx/xxxxx.
    Precondition: emoji_list must be a list in which each element is a string of the
                  form u+xxxx/xxxxx.
    """
    uni_list = []
    for potential in emoji_list:
        uni_list.append("eoji" + potential[2:])
    return uni_list