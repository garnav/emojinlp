#pull.py
#Arnav Ghosh
#21th June 2016

import watson_developer_cloud
import json
import requests
import emoji_list
import re

#Alchemy API
API_KEY = "4e0c227f98c57761752ae03bfecd1a73ddece3ca"

#known emojisaurus discrepencies (either not a shortcode, or not a twitter recognized shortcode used on the site)
DSC = {
    ':tshirt:':':shirt:',
    ':thumbsup:':':+1:',
    ':thumbsdown:':':-1:',
    ':running:':':runner:',
    ':date:':':calendar:',
    ':tshirt:':':shirt:',
    ':pencil:':':pencil2:',
    ':honeybee:':':bee:',
    ':poop:':':hankey:',
    ':radio_button:':':radio:',
    ':punch:':':facepunch:',
    }

SENT_LABELS = {"positive":1, "negative":-1, "neutral":0}

def getSent(json_file):
    """Returns a list of sentences from a file containing the output of an
    IBM Watson Document Conversion (as answer_units) on 
    Preconditions: json_file is a JSON file containing the output of an IBM Watson
                   Document Conversion call with the configuration target as
                   answer_units. 
    """
    opened = open(json_file)
    read_str = opened.read()
    
    #have to use a dictionary not a string
    read_json = json.loads(read_str)
    
    answers = read_json["answer_units"]
    recovered =  []
    
    for ans in answers:
        #maybe should check by a certain number of emojis instead
        if len(ans["title"]) > 15:
            recovered.append(ans["title"])

    return recovered

def translation(html_in, sent_list):
    """Returns a dictionary containing a sentence mapped to a list of ordered emoji unicodes as an emoji
    translation.
    Preconditions: html_in is a .txt file containing an html document from
                   sent_list is a list of strings, each representing a sentence to translate. 
    """
    opened = open(html_in)
    read_str = opened.read()
    translations = {}
    discarded = []
    not_found = []
    
    for sentence in sent_list:
        #Possibly used non-ascii keys in the sentence which are of no use
        try:
            #need it in ascii (is this correct?0)
            sentence = sentence.encode()
            
            ind = read_str.find(sentence)
            #find a better way to do this
            endblock = read_str.find("</div>", ind)
            new_block = read_str[ind:endblock]

            trans = []
            while new_block.find("data-emoji-name") != -1:
                ind = new_block.find("data-emoji-name")
                equal = new_block.find("=", ind)
                name = new_block[equal + 2 : new_block.find('"', equal + 2)]
                name = ":" + name + ":"
               
                #only for development, check which names aren't there
                try:
                    converted = shortUni(name)
                    trans.append(converted)
                    #print trans
                except:
                    if name not in not_found:
                        print sentence, name
                        not_found.append(name)
                new_block = new_block[ind + 1:]
                
            if len(trans) >= 3:    
                translations[sentence] = trans
        except UnicodeEncodeError:
            discarded.append(sentence)
           
    print not_found       
    return translations

def shortUni(short_code):
    """Returns a unicode representation of a corresponding emoji shortcode. A unicode representation
    is of the form u+xxxx/xxxxx where xxxxx represents hex code.
    Preconditions: short_code must an ASCII string of the form :accepted_twitter_short_code:
    """
    if short_code in DSC.keys():
        short_code = DSC[short_code]
    uni = emoji_list.EMOJI_UNICODE[short_code]
    return 'u+' + uni.lower()

def alchemySentiment(send_dict):
    """
    """
    alchemy_language = watson_developer_cloud.AlchemyLanguageV1(api_key=API_KEY)
    sentences = send_dict.keys()
    emoji_sentiment = {}
    
    for send_text in sentences:
        try:
            results = alchemy_language.combined(text=send_text, extract=["doc-sentiment"])
            sentiment = results["docSentiment"]["type"]
            emoji_sentiment[tuple(send_dict[send_text])] = SENT_LABELS[sentiment]
        except watson_developer_cloud.watson_developer_cloud_service.WatsonException:
            print send_text
    
    return emoji_sentiment